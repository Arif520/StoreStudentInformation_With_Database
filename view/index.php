<?php

require_once "../src/Student.php";


$studentObj = new Student();

$allData = $studentObj->index();


for($i=0;$i<count($allData);$i++){

    $banglaGrade = $studentObj->mark2Grade($allData[$i]->bangla_mark);
    $englishGrade = $studentObj->mark2Grade($allData[$i]->english_mark);
    $mathGrade = $studentObj->mark2Grade($allData[$i]->math_mark);


    $template = file_get_contents("ShowDataTemplate.html");

    $template = str_replace("#$#firstName#$#",$allData[$i]->first_name,$template);
    $template = str_replace("#$#lastName#$#",$allData[$i]->last_name,$template);
    $template = str_replace("#$#roll#$#",$allData[$i]->roll,$template);
    $template = str_replace("#$#dob#$#",$allData[$i]->dob,$template);
    $template = str_replace("#$#gender#$#",$allData[$i]->gender,$template);
    $template = str_replace("#$#banglaMark#$#",$allData[$i]->bangla_mark,$template);
    $template = str_replace("#$#englishMark#$#",$allData[$i]->english_mark,$template);
    $template = str_replace("#$#mathMark#$#",$allData[$i]->math_mark,$template);
    $template = str_replace("#$#banglaGrade#$#",$banglaGrade,$template);
    $template = str_replace("#$#englishGrade#$#",$englishGrade,$template);
    $template = str_replace("#$#mathGrade#$#",$mathGrade,$template);


    echo $template;
}

