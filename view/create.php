<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Information Collection Form</title>
    <link rel="stylesheet" href="../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <script src="../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <style>
        #GenderDiv{
            position: relative;
            left: 100px;
            margin-top: 40px;
        }

    </style>
</head>
<body>


<div class="container">
    <h1>Student's Information Collection Form</h1>

    <form action="store.php" method="post">
        <div class="form-group">
            <label for="FirstName">First Name</label>
            <input required type="text" class="form-control" name="FirstName" placeholder="Enter First Name Here...">
        </div>

        <div class="form-group">
            <label for="LastName">Last Name</label>
            <input required type="text" class="form-control" name="LastName" placeholder="Enter Last Name Here...">
        </div>


           <div style="position: relative; left:-13px" class="col-lg-4">
               <label for="Roll">Roll</label>
               <input required type="text" class="form-control" name="Roll" placeholder="Enter Roll Here...">
           </div>



           <div style="position: relative; left:13px" class="col-lg-4">
               <label for="DOB">Date of Birth</label>
               <input required type="date" class="form-control" id="DOB" name="DOB">
           </div>


        <div id="GenderDiv" class="form-group">
            <label for="Gender">Gender</label>
            <input type="radio" id="Gender" name="Gender" value="Male"> Male
            <input required type="radio"  id="Gender" name="Gender" value="Female"> Female
        </div>

        <p>
            &nbsp;
        </p>






        <div style="position: relative; left:-13px" class="col-lg-4">
            <label for="BanglaMark">Bangla Mark</label>
            <input required type="text" class="form-control" name="BanglaMark" placeholder="Enter Bangla Mark Here...">
        </div>


        <div style="position: relative; left:13px" class="col-lg-4">
            <label for="EnglishMark">English Mark</label>
            <input required type="text" class="form-control" name="EnglishMark" placeholder="Enter English Mark Here...">
        </div>


        <div style="position: relative; left:13px" class="col-lg-4">
            <label for="MathMark">Math Mark</label>
            <input required type="text" class="form-control" name="MathMark" placeholder="Enter Math Mark Here...">
        </div>


        <p>
            &nbsp;
        </p>


        <button type="submit" class="btn btn-default">Submit</button>
    </form>

</div>


</body>
</html>