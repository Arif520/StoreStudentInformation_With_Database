-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 22, 2018 at 01:39 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `batch2_php_db1`
--
CREATE DATABASE IF NOT EXISTS `batch2_php_db1` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `batch2_php_db1`;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(13) NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `roll` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bangla_mark` float NOT NULL,
  `english_mark` float NOT NULL,
  `math_mark` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `first_name`, `last_name`, `gender`, `dob`, `roll`, `bangla_mark`, `english_mark`, `math_mark`) VALUES
(1, 'Code', 'Station', 'Male', '2018-06-20', '234526', 35, 64, 56),
(2, 'rakib', 'hasan', 'Male', '2018-06-10', '45363', 45, 86, 75),
(3, 'Arifur', 'Rahman', 'Male', '2018-06-19', '437', 35, 64, 56),
(4, 'Popy', 'Dey', 'Female', '2018-06-09', '34', 45, 86, 75),
(5, 'Moktar', 'Hossain', 'Male', '2018-06-19', '2er5464', 46, 98, 56),
(6, 'gfhfhg', 'vfbfhg', 'Male', '2018-06-18', '564', 57, 464, 34),
(7, 'xbcbh', 'cghcgh', 'Male', '2018-06-07', '4545', 67, 97, 67),
(8, 'xbcbh', 'cghcgh', 'Male', '2018-06-07', '4545', 67, 97, 67),
(9, 'asfd555', 'hh3cghcgh', 'Female', '2018-06-09', 'uyr34', 56, 77, 65),
(10, 'Ayra', 'Chowdhury', 'Male', '2018-03-23', '34', 53, 66, 44),
(11, 'Moktar', 'Hossain', 'Male', '2000-01-04', '3489730947', 88, 55, 99);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
