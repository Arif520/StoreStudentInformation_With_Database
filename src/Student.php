<?php
require_once "Database.php";

class Student extends Database
{


    protected $firstName;
    protected $lastName;
    protected $dob;
    protected $gender;


    protected $roll;
    protected $banglaMark;
    protected $englishMark;
    protected $mathMark;

    public function setData($postArray){

        if(array_key_exists("FirstName",$postArray) ){
            $this->firstName = $postArray["FirstName"];

        }


        if(array_key_exists("LastName",$postArray) ){
            $this->lastName = $postArray["LastName"];
        }
        if(array_key_exists("Roll",$postArray) ){
            $this->roll = $postArray["Roll"];
        }

        if(array_key_exists("DOB",$postArray) ){
            $this->dob = $postArray["DOB"];
        }

        if(array_key_exists("Gender",$postArray) ){
            $this->gender = $postArray["Gender"];
        }

        if(array_key_exists("BanglaMark",$postArray) ){
            $this->banglaMark = $postArray["BanglaMark"];
        }

        if(array_key_exists("EnglishMark",$postArray) ){
            $this->englishMark = $postArray["EnglishMark"];
        }

        if(array_key_exists("MathMark",$postArray) ){
            $this->mathMark = $postArray["MathMark"];
        }



    }// end of setStudentData()



   public function store(){

       $sqlStatement = "INSERT INTO `students` (`id`, `first_name`, `last_name`, `gender`, `dob`, `roll`, `bangla_mark`, `english_mark`, `math_mark`) VALUES (NULL, '$this->firstName', '$this->lastName','$this->gender' , '$this->dob', '$this->roll', '$this->banglaMark', '$this->englishMark', '$this->mathMark');";

      $result = $this->DBH->exec($sqlStatement);

      if($result){
          echo "Data has been inserted successfully<br>";
      }
      else{
          echo "Data has not been inserted successfully<br>";
      }

   }// end of store();




    public function index(){

        $sqlStatement = "Select * from students";

        $STH = $this->DBH->query($sqlStatement);

        $allData = $STH->fetchAll(PDO::FETCH_OBJ);

        return $allData;

    }// end of index()



    public function mark2Grade($mark){

        if($mark>=80) return "A+";
        elseif($mark>=70) return "A";
        elseif($mark>=60) return "B";
        elseif($mark>=50) return "C";
        elseif($mark>=40) return "D";
        else return "F";

    }

}

